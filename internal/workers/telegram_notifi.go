package workers

import (
	"context"
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/service/message_brocker"
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/service/telegram"
	"go.uber.org/zap"
	"time"
)

type TelegramNotifier struct {
	messageBroker  message_brocker.Queuer
	telegramSender telegram.Messenger
}

func NewTelegramBotWorker(messageBroker message_brocker.Queuer, telegramSender telegram.Messenger) *TelegramNotifier {
	return &TelegramNotifier{messageBroker: messageBroker, telegramSender: telegramSender}
}

func (e *TelegramNotifier) Run(ctx context.Context, log *zap.Logger) {
	msgs, err := e.messageBroker.Get()
	if err != nil {
		log.Fatal("Getting MessageBroker data error", zap.Error(err))
		return
	}
	go func() {
		for msg := range msgs {
			err = e.telegramSender.SendMessage(string(msg.Body))
			if err != nil {
				log.Error("Sending message error", zap.Error(err))
			}
			select {
			case <-ctx.Done():
				return
			default:
			}
		}
		time.Sleep(time.Millisecond * 200)
	}()
}

package docs

import (
	rcontroller "gitlab.com/lear4212225/gateaway/internal/modules/rates/controller"
)

// swagger:route GET /api/1/rates/ticker rates tickerRequest
// Получение информации о текущей цене криптовалют.
// security:
//   - Bearer: []
// responses:
//   200: tickerResponse

// swagger:response tickerResponse
type tickerResponse struct {
	// in:body
	Body rcontroller.GetTickerResponse
}

// swagger:route GET /api/1/rates/minPrices rates pricesMinRequest
// Получение информации о минимальных цен криптовалют.
// security:
//   - Bearer: []
// responses:
//   200: pricesMinResponse

// swagger:response pricesMinRequest
type pricesMinRequest struct {
}

// swagger:response pricesMinResponse
type pricesMinResponse struct {
	// in:body
	Body rcontroller.GetPricesResponse
}

// swagger:route GET /api/1/rates/maxPrices rates pricesMaxRequest
// Получение информации о максимальных цен криптовалют.
// security:
//   - Bearer: []
// responses:
//   200: pricesMaxResponse

// swagger:response pricesMaxRequest
type pricesMaxRequest struct {
}

// swagger:response pricesMaxResponse
type pricesMaxResponse struct {
	// in:body
	Body rcontroller.GetPricesResponse
}

// swagger:route GET /api/1/rates/avgPrices rates pricesRequest
// Получение информации о средних цен криптовалют.
// security:
//   - Bearer: []
// responses:
//   200: pricesResponse

// swagger:response pricesRequest
type pricesRequest struct {
}

// swagger:response pricesResponse
type pricesResponse struct {
	// in:body
	Body rcontroller.GetPricesResponse
}

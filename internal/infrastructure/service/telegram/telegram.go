package telegram

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/lear4212225/gateaway/config"
	"go.uber.org/zap"
	"strconv"
)

type Messenger interface {
	SendMessage(text string) error
}

type Telegram struct {
	bot    *tgbotapi.BotAPI
	chatID int64
}

func NewTelegram(api config.TelegramAPI, logger *zap.Logger) Messenger {
	bot, err := tgbotapi.NewBotAPI(api.Key)
	if err != nil {
		logger.Fatal("NewBotAPI error", zap.Error(err))
	}

	id, err := strconv.Atoi(api.ChatID)
	if err != nil {
		logger.Fatal("strconv.Atoi error", zap.Error(err))
		return nil
	}

	return &Telegram{
		bot:    bot,
		chatID: int64(id),
	}
}

func (c *Telegram) SendMessage(text string) error {
	msg := tgbotapi.NewMessage(c.chatID, text)
	_, err := c.bot.Send(msg)
	return err
}

package message_brocker

import (
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/lear4212225/gateaway/config"
	"go.uber.org/zap"
)

type Queuer interface {
	Get() (<-chan amqp.Delivery, error)
}

type RabbitMQ struct {
	ch    *amqp.Channel
	queue *amqp.Queue
}

func NewRabbitMQ(rabbit config.RabbitMQ, logger *zap.Logger) Queuer {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", rabbit.Login, rabbit.Pass, rabbit.Host, rabbit.Port))
	if err != nil {
		logger.Fatal(err.Error())
	}
	ch, err := conn.Channel()
	if err != nil {
		logger.Fatal(err.Error())
	}

	q, err := ch.QueueDeclare(
		"exchangeTg", // name
		false,        // durable
		false,        // delete when unused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)

	return &RabbitMQ{
		ch:    ch,
		queue: &q,
	}
}

func (p *RabbitMQ) Get() (<-chan amqp.Delivery, error) {
	msg, err := p.ch.Consume(
		p.queue.Name, // queue
		"",           // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
	return msg, err
}

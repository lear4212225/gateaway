package controller

import (
	"net/http"

	"github.com/ptflp/godecoder"
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/component"
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/errors"
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/responder"
	service "gitlab.com/lear4212225/gateaway/internal/infrastructure/service/rates"
)

type Rater interface {
	GetTicker(w http.ResponseWriter, r *http.Request)
	GetMinPrices(w http.ResponseWriter, r *http.Request)
	GetMaxPrices(w http.ResponseWriter, r *http.Request)
	GetAveragePrices(w http.ResponseWriter, r *http.Request)
}

type Rate struct {
	rates service.RatesClient
	responder.Responder
	godecoder.Decoder
}

func NewRates(service service.RatesClient, components *component.Components) Rater {
	return &Rate{rates: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (rs *Rate) GetTicker(w http.ResponseWriter, r *http.Request) {
	ticker, err := rs.rates.GetTicker(r.Context(), &service.GetTickerIn{})
	if err != nil {
		msg := "getting ticker error"
		rs.OutputJSON(w, GetTickerResponse{
			Success:   false,
			ErrorCode: errors.RatesServiceGettingTickerErr,
			Data: Data{
				Message: msg,
			},
		})
		return
	}

	rs.OutputJSON(w, GetTickerResponse{
		Success: true,
		Data: Data{
			Message: "ticker information",
			Rates:   ticker.Rates,
		},
	})
}

func (rs *Rate) GetMinPrices(w http.ResponseWriter, r *http.Request) {
	prices, err := rs.rates.GetMinPrices(r.Context(), &service.GetMinPricesIn{})
	if err != nil {
		msg := "getting min prices error"
		rs.OutputJSON(w, GetPricesResponse{
			Success:   false,
			ErrorCode: errors.RatesServiceGettingMinPriceErr,
			Data: PData{
				Message: msg,
			},
		})
		return
	}

	rs.OutputJSON(w, GetPricesResponse{
		Success: true,
		Data: PData{
			Message: "min prices information",
			Prices:  prices.MinPrices,
		},
	})
}

func (rs *Rate) GetMaxPrices(w http.ResponseWriter, r *http.Request) {
	prices, err := rs.rates.GetMaxPrices(r.Context(), &service.GetMaxPricesIn{})
	if err != nil {
		msg := "getting max prices error"
		rs.OutputJSON(w, GetPricesResponse{
			Success:   false,
			ErrorCode: errors.RatesServiceGettingMaxPriceErr,
			Data: PData{
				Message: msg,
			},
		})
		return
	}

	rs.OutputJSON(w, GetPricesResponse{
		Success: true,
		Data: PData{
			Message: "max prices information",
			Prices:  prices.MaxPrices,
		},
	})
}

func (rs *Rate) GetAveragePrices(w http.ResponseWriter, r *http.Request) {
	prices, err := rs.rates.GetAveragePrices(r.Context(), &service.GetAveragePricesIn{})
	if err != nil {
		msg := "getting average prices error"
		rs.OutputJSON(w, GetPricesResponse{
			Success:   false,
			ErrorCode: errors.RatesServiceGettingAvgPriceErr,
			Data: PData{
				Message: msg,
			},
		})
		return
	}

	rs.OutputJSON(w, GetPricesResponse{
		Success: true,
		Data: PData{
			Message: "average prices information",
			Prices:  prices.AvgPrices,
		},
	})
}

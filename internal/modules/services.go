package modules

import (
	aservice "gitlab.com/lear4212225/gateaway/internal/infrastructure/service/auth"
	rservice "gitlab.com/lear4212225/gateaway/internal/infrastructure/service/rates"
	uservice "gitlab.com/lear4212225/gateaway/internal/infrastructure/service/user"
)

type Services struct {
	User  uservice.Userer
	Auth  aservice.Auther
	Rates rservice.RatesClient
}

func NewServices(userService uservice.Userer, authService aservice.Auther, ratesService rservice.RatesClient) *Services {
	return &Services{
		User:  userService,
		Auth:  authService,
		Rates: ratesService,
	}
}

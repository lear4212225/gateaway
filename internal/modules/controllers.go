package modules

import (
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/component"
	acontroller "gitlab.com/lear4212225/gateaway/internal/modules/auth/controller"
	rcontroller "gitlab.com/lear4212225/gateaway/internal/modules/rates/controller"
	ucontroller "gitlab.com/lear4212225/gateaway/internal/modules/user/controller"
)

type Controllers struct {
	Auth  acontroller.Auther
	User  ucontroller.Userer
	Rates rcontroller.Rater
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	rateController := rcontroller.NewRates(services.Rates, components)

	return &Controllers{
		Auth:  authController,
		User:  userController,
		Rates: rateController,
	}
}

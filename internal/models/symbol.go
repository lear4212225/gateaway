package models

type Symbol struct {
	BuyPrice  string `json:"buy_price,omitempty"`
	SellPrice string `json:"sell_price,omitempty"`
	LastTrade string `json:"last_trade,omitempty"`
	High      string `json:"high,omitempty"`
	Low       string `json:"low,omitempty"`
	Avg       string `json:"avg,omitempty"`
	Vol       string `json:"vol,omitempty"`
	VolCurr   string `json:"vol_curr,omitempty"`
	Updated   int64  `json:"updated,omitempty"`
}

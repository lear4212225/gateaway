package storages

import (
	"gitlab.com/lear4212225/gateaway/internal/db/adapter"
	"gitlab.com/lear4212225/gateaway/internal/infrastructure/cache"
	vstorage "gitlab.com/lear4212225/gateaway/internal/modules/auth/storage"
	ustorage "gitlab.com/lear4212225/gateaway/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
